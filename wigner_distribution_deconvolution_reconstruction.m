function [ptycho] = wigner_distribution_deconvolution_reconstruction(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To perform the Wigner Distrubution Deconvolution reconstruction
% Function requires structured array ptycho.
% this function needs comments

% ptycho.varfunctions.wigner_distribution_deconvolution_reconstruction: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% wigner distribution function of probe

% centre of Q_vector
[~,qy0] = min(abs(ptycho.Q_y));
[~,qx0] = min(abs(ptycho.Q_x));

% centre of K vector
[~,ky0] = min(abs(ptycho.ky_wp));
[~,kx0] = min(abs(ptycho.kx_wp));

% Aperture function (top hat)
func_ObjApt = ones(size(ptycho.Kwp));
func_ObjApt( ptycho.Kwp > ptycho.ObjApt_angle) = 0;
func_ObjApt = func_ObjApt .* ptycho.scaling;
        
% Wiener filter constant
% auto-correlation function of A
chia = fftshift(ifft2(ifftshift( ptycho.A.* conj(ptycho.A) )));

epsilon = ptycho.eps_ratio * max(abs(chia(:)))^2;

% figure; imagesc(log(epsilon+abs(chia).^2));axis image; colorbar
% caxis([0 max(log(epsilon+abs(chia(:)).^2))])

% allocate memory for D = Psi(Kf)Psi*(Kf-Qp)
% D = zeros(size(H));

D = zeros(ptycho.ObjSize);

hh = waitbar(0,'WDD main process started...');
for yy=1:size(ptycho.H,3)
    perc = yy/ptycho.ObjSize(1);
    waitbar(perc,hh,sprintf('WDD: %d%% along...',round(perc*100)));
    
    for xx = 1:size(ptycho.H,4)

        Qx_rot = ptycho.Q_x(xx).*cos(ptycho.rot_angle) - ptycho.Q_y(yy).*sin(ptycho.rot_angle);
        Qy_rot = ptycho.Q_x(xx).*sin(ptycho.rot_angle) + ptycho.Q_y(yy).*cos(ptycho.rot_angle);  
        
        Kx_wp_plusQ  = ptycho.Kx_wp + Qx_rot;
        Ky_wp_plusQ  = ptycho.Ky_wp + Qy_rot;
        Kwp_plusQ = sqrt(Kx_wp_plusQ.^2 + Ky_wp_plusQ.^2);
        
        Kx_wp_minusQ = ptycho.Kx_wp - Qx_rot;
        Ky_wp_minusQ = ptycho.Ky_wp - Qy_rot;
        Kwp_minusQ = sqrt(Kx_wp_minusQ.^2 + Ky_wp_minusQ.^2);
        
        func_aber = FuncAberrUV(ptycho.Kx_wp,ptycho.Ky_wp,ptycho.aberr_input);
        func_aber_plusQ = FuncAberrUV(Kx_wp_plusQ,Ky_wp_plusQ,ptycho.aberr_input);
        func_aber_minusQ = FuncAberrUV(Kx_wp_minusQ,Ky_wp_minusQ,ptycho.aberr_input);
        
        func_transfer=exp(-sqrt(-1)*2*pi/ (ptycho.wavelength*1e-10) .* func_aber);
        func_transfer_plusQ = exp(-sqrt(-1)*2*pi/ (ptycho.wavelength*1e-10) .* func_aber_plusQ);
        func_transfer_minusQ = exp(-sqrt(-1)*2*pi/ (ptycho.wavelength*1e-10) .* func_aber_minusQ);
        
        func_ObjApt_plusQ = ones(size(Kwp_plusQ));
        func_ObjApt_plusQ( Kwp_plusQ > ptycho.ObjApt_angle) = 0;
%         func_ObjApt_plusQ( Kwp_minusQ < ptycho.ObjApt_angle) = 0;
%         func_ObjApt_plusQ( Kwp > ptycho.ObjApt_angle) = 0;
        
        func_ObjApt_minusQ = ones(size(Kwp_minusQ));
        func_ObjApt_minusQ( Kwp_minusQ > ptycho.ObjApt_angle) = 0;
%         func_ObjApt_minusQ( Kwp_plusQ < ptycho.ObjApt_angle) = 0;
%         func_ObjApt_minusQ( Kwp > ptycho.ObjApt_angle) = 0;
        
        A = func_ObjApt.*func_transfer;
        A_KfplusQ   = func_ObjApt_plusQ  .* ptycho.scaling .*  func_transfer_plusQ ;
        A_KfminusQ  = func_ObjApt_minusQ .* ptycho.scaling .*  func_transfer_minusQ;

 
        if ptycho.ChrDeconvFlag
            AA = A.* conj(A_KfminusQ) .* exp( ConstA.*(2*(ptycho.Ky_wp.*Qy_rot+ptycho.Kx_wp.*Qx_rot)+Qy_rot^2+Qx_rot^2).^2 );
        else
            AA = A.* conj(A_KfminusQ);
        end
        
        chia = fftshift(ifft2(ifftshift( AA )));
        chia_mod = abs(chia);
        chipsi = conj(chia) .* squeeze(ptycho.H(:,:,yy,xx)) ./ (chia_mod.*chia_mod + epsilon);
%       D(:,:,yy,xx) = fftshift(fft2(ifftshift( chipsi )));

        d = fftshift(fft2(ifftshift( chipsi )));
        D(yy,xx) = d(ky0,kx0);
        
    end
end
close(hh)

Psi = D ./ sqrt(conj(D(qy0,qx0)));
% fourier transform of Psi to obtain the object function in real space.
psi = fftshift(ifft2(ifftshift( Psi )));

ptycho.Psi = Psi;
ptycho.psi = psi;
ptycho.D = D;
ptycho.varfunctions.wigner_distribution_deconvolution_reconstruction = 1;

if ptycho.plot_figures
%    show_WDD_results(ptycho)
    figure;
    subplot(1,2,1);
    imagesc(abs(ptycho.psi));
    axis image; colorbar; colormap gray;
    title(['WDD Modulus, \epsilon = ',num2str(ptycho.eps_ratio)])
    subplot(1,2,2);
    imagesc(angle((ptycho.psi)));
    axis image; colorbar; colormap gray
    title(['WDD Phase, \epsilon = ',num2str(ptycho.eps_ratio)])
end


end

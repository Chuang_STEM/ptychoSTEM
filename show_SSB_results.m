function [] =  show_SSB_results(ptycho)
%To show SSB results

% this function needs comments + add figure labels

    figure
    subplot(1,2,1)
    imagesc(ptycho.X0,ptycho.Y0,abs(ptycho.trotterimgL));
    axis image; colorbar;colormap gray
    title('Amplitude')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    subplot(1,2,2)
    imagesc(ptycho.X0,ptycho.Y0,angle(ptycho.trotterimgL));
    axis image; colorbar;colormap gray
    title('Phase')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    set(gcf,'color','w');
    
    figure
    subplot(1,2,1)
    imagesc(ptycho.X0,ptycho.Y0,abs(ptycho.trotterimgR));
    axis image; colorbar; colormap gray
    title('Amplitude')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    subplot(1,2,2)
    imagesc(ptycho.X0,ptycho.Y0,angle(ptycho.trotterimgR)); 
    axis image; colorbar; colormap gray
    title('phase')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    set(gcf,'color','w');
    
    if ptycho.varfunctions.show_synthetic_IBF
    figure
    imagesc(ptycho.X0,ptycho.Y0,1-ptycho.IBFimg./max(ptycho.IBFimg(:)));
    axis image; colorbar; colormap gray
    title('1-IBF')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    figure
    imagesc(ptycho.X0,ptycho.Y0,abs(ptycho.ABFimg));
    axis image; colorbar; colormap gray
    title('ABF')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    figure
    imagesc(ptycho.X0,ptycho.Y0,abs(ptycho.trotterimgR));
    axis image; colorbar; colormap gray
    title('Amplitude')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    figure
    imagesc(ptycho.X0,ptycho.Y0,angle(ptycho.trotterimgR)); axis image; colorbar; colormap gray
    title('phase')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    set(gcf,'color','w');
    colormap gray
    
    figure
    subplot(2,2,1)
    % imagesc(ptycho.X0,ptycho.Y0,1-ptycho.IBFimg./max(ptycho.IBFimg(:))); axis image; colorbar; colormap gray
    % title('1-IBF')
    imagesc(ptycho.X0,ptycho.Y0,ptycho.DFimg);
     title('DF')
    axis image; colorbar; colormap gray
    xlabel('angstrom');ylabel('angstrom');
    axis off
    subplot(2,2,2)
    imagesc(ptycho.X0,ptycho.Y0,abs(ptycho.ABFimg)); axis image; colorbar; colormap gray
    title('ABF')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    subplot(2,2,3)
    imagesc(ptycho.X0,ptycho.Y0,angle(ptycho.trotterimgL)); axis image; colorbar; colormap gray
    title('phase')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    subplot(2,2,4)
    imagesc(ptycho.X0,ptycho.Y0,angle(ptycho.trotterimgR)); axis image; colorbar; colormap gray
    title('phase')
    xlabel('angstrom');ylabel('angstrom');
    axis off
    set(gcf,'color','w');
    colormap gray
    end
%     figure
%     subplot(1,2,1)
%     imagesc(ptycho.X0,ptycho.Y0,abs(ptycho.trotterimgR)); axis image; colorbar; colormap gray
%     title('Amplitude')
%     xlabel('angstrom');ylabel('angstrom');
%     axis off
    % subplot(1,2,2)
    % imagesc(ptycho.X0,ptycho.Y0,angle(trotterimgR)./Sigma(200)); axis image; colorbar; colormap gray
    % title('Projected Potential [Volt*Angstrom^2]')
    % xlabel('angstrom');ylabel('angstrom');
    % axis off
    % set(gcf,'color','w');
    %
    % [tpx,tpy] = gradient(angle(trotterimgR)./Sigma(200));
    % figure;
    % % imagesc(ptycho.IBFimg); axis image; colormap gray;  axis off;
    % % hold on
    % h=quiver(tpx,tpy);
    % h.Color = 'blue';
    % axis tight;
    % set(gca,'YDir','reverse');
    % axis image;
    % axis off
    % xlim([1 size(m,4)]); ylim([1 size(m,3)]);
    % hold off
    
    % figure;
    % imagesc(sqrt(tpx.^2+tpy.^2));
    % axis tight;
    % set(gca,'YDir','reverse');
    % axis image;
    % axis off
    % colormap gray

end
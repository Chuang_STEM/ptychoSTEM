% 
% %rest mass of electrons
% m0 = 510.99891013; % keV
% e =14.4; % Volt-Angstrom
% % Planck constant
% h = 4.135667517e-18; %keV*s  
% % Bohr radius: a0 = hbar*hbar/(m0*e*e) - Gaussian Unit;
% a0 = 0.5291772083; % Angstrom;
% c = 3e18; %speed of light, Angstrom/s
% 
% % interaction constant defined in JMCowley's Electron Diffraction Techniques.
% sigma = wavelength/sqrt(1+h^2/(m0^2*c^2*wavelength^2))/(2*pi*a0^2*e); 
% % unit in 1/(Volt*Angstrom)


function s = Sigma( kev )

    
% /*--------------------- sigma() -----------------------------------*/
%         return the interaction parameter sigma in radians/(Volt-Angstroms)
%         keep this in one place so I don't have to keep typing in these
%         constants (that I can never remember anyhow)
% 
%         ref: Physics Vade Mecum, 2nd edit, edit. H. L. Anderson
%                 (The American Institute of Physics, New York) 1989
%                 page 4.
% 
%         kev = electron energy in keV
% /*--------------------- ------ -----------------------------------*/


  emass=510.99906; %/* electron rest mass in keV */
  
  x = ( emass + kev ) / ( 2.0*emass + kev);  
  wavl = Wavelength( kev );
%   pi = 4.0 * atan( 1.0 );
  
  % s in radians/(kV-Angstron)
  s = 2.0 * pi * x / (wavl*kev); % // 2*pi*kz*(1+kev/emaxx)/(2*emass+kev)
  % s in radians/(Volt-Angstrom);
  s = s/1000;
end
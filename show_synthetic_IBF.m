function [ptycho] = show_synthetic_IBF(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To calculate and show synthethic Incoherent Bright Field image. 
% Function requires structured array ptycho. Figures are displayed if
% ptycho.plot_figures is set to 1. 
% Input variables required are:
% ptycho.m: 4D array that contains the ronchigram for each probe position
% ptycho.pacbed: matrix containing the Position Averaged Convergent Beam
% Electron Diffraction (PACBED) pattern.
% ptycho.maskIBF: matrix of mask for Incoherent Bright Field detector
% If ptycho.maskIBF is not available, it will calculate it. This will require the
% following variables:
%   ptycho.ObjApt_angle: Probe convergence angle
%   ptycho.theta: matrix of scattering angles
% Output variables added to ptycho are:
% ptycho.IBFimg: matrix of Incoherent Bright Field image
% ptycho.varfunctions.show_synthetic_IBF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ptycho.varfunctions.show_synthetic_IBF %check if function has been run
    
    figure;
    imagesc(ptycho.IBFimg); axis image; axis off; title('IBF')
    colormap gray
    
else
    if ptycho.varfunctions.calculate_detector_masks
        maskIBF = ptycho.maskIBF;
    else
        ptycho = calculate_detector_masks(ptycho);
        maskIBF = ptycho.maskIBF;
    end   
    
    % calculate the IBF image
    IBFimg = zeros(size(ptycho.m,3),size(ptycho.m,4));
    
    for yy=1:size(ptycho.m,3)
        for xx = 1:size(ptycho.m,4)
            %IBF
            img = ptycho.m(:,:,yy,xx).*maskIBF;
            IBFimg(yy,xx) = sum(img(:));
        end
    end
    
    if ptycho.plot_figures
        
        figure;
        imagesc(ptycho.pacbed.*maskIBF);axis image;
        title('IBF detector')
        
        figure;
        imagesc(IBFimg); axis image; axis off; title('IBF')
        colormap gray
    end
    
    ptycho.IBFimg = IBFimg;
    ptycho.varfunctions.show_synthetic_IBF = 1;
    
end

end

